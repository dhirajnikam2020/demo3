#! /bin/bash -x

# running inteegration tests
mvn test -DintegrationTests=true

if [[ $? -eq 0 ]]; then
  echo "integration test succeeded"
else
  echo "integration test fails. Rolling back deployment to previous version!!"
  ci-cd/scripts/rollback-deployment.sh
fi;
