#!/bin/bash -x

# set docker image name
IMAGE_NAME=${APPNAME}:${CI_COMMIT_SHORT_SHA}.${CI_PIPELINE_IID}

# Build the application
mvn --batch-mode install || exit $?


# Create docker image and tagged it with commit-id and pipeline-id (Unique tag)
docker build -t ${IMAGE_NAME} . || exit $?

# Push Docker image to docker repository/artifactory
docker push ${IMAGE_NAME} || exit $?
