#! /bin/bash -x
# @brief Validate deployment of microservices to kubernetes and rollback in case of failed deployment.
# if fails then it will rollback

WORKDIR=$(pwd)
KUBE_CFGDIR=$(dirname $KUBECONFIG)

checking_deployed_status() {
  docker run -i --rm \
  -v ${WORKDIR}:${WORKDIR}:ro -w ${WORKDIR} \
  -v ${KUBE_CFGDIR}:${KUBE_CFGDIR}:ro \
  -e KUBE_URL -e KUBE_NAMESPACE -e KUBECONFIG \
  --network=host \
  alpine/helm:${HELM_VERSION} status ${HELMCHARTNAME} | grep 'STATUS:' | awk '{print $2}'
}
deployed_status="DEPLOYED"

checking_resource_status() {
  docker run -i --rm \
  -v ${KUBE_CFGDIR}:${KUBE_CFGDIR}:ro \
  alpine/k8s:1.11.10 \
  kubectl --kubeconfig=${KUBE_CFGDIR}/KUBECONFIG \
  get all | grep "deployment.apps/${HELMCHARTNAME}" | awk '{print $2}'
}
resource_status="1/1"


function wait_for {
  local command_output=$1
  local min_attempt=0
  local max_attempt=20
  local delay_in_sec=20
  while [[ $min_attempt -lt $max_attempt ]]; do
    {
      if [[ $deployed_status == ${command_output} ]]; then
        command_input_tmp="$(checking_deployed_status)"
        command_input=${command_input_tmp^^}
      elif [[ $resource_status == $command_output ]]; then
        result="$(checking_resource_status)"
        command_input=$(($result))/1
      fi

      if [[ $command_input == $command_output ]]; then
        echo "check completes.."
        return 0
      else
        sleep $delay_in_sec;
      fi
        ((min_attempt++))
        echo "Retrying again, Attempt $min_attempt/$max_attempt"
    }
  done
  if [[ $min_attempt -ge $max_attempt ]]; then
    echo "command has failed after $max_attempt attempts. Rolling back deployment to previous version!!"
    ci-cd/scripts/rollback-deployment.sh
    exit 1
  fi
  return 2
}

# Deploy database befoe application if its set to deploy true
if [ ! -z "${DEPLOY_DB}" ]; then
  HELMCHARTNAME=${DB_NAME}
  wait_for $deployed_status
  wait_for $resource_status

  HELMCHARTNAME=${APPNAME}
  wait_for $deployed_status
  wait_for $resource_status

else
  HELMCHARTNAME=${APPNAME}
  wait_for $deployed_status
  wait_for $resource_status
fi;
