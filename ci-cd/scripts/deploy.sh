#! /bin/bash -x
# @brief Deploy a microservice to kubernetes.


WORKDIR=$(pwd)

# set docker image name
IMAGE_NAME=${APPNAME}:${CI_COMMIT_SHORT_SHA}.${CI_PIPELINE_IID}

# GitLab stores config files in a tmp dir, but doesn't declare that dir as
# variable :/
KUBE_CFGDIR=$(dirname $KUBECONFIG)

# Run helm from a docker image matching the helm version installed in the environment.
deployDB() {
  HELMCHARTNAME=$1

  docker run -i --rm \
    -v ${WORKDIR}:${WORKDIR}:ro -w ${WORKDIR} \
    -v ${KUBE_CFGDIR}:${KUBE_CFGDIR}:ro \
    -e KUBE_URL -e KUBE_NAMESPACE -e KUBECONFIG \
    --network=host \
    alpine/helm:${HELM_VERSION} \
    upgrade --install \
    --namespace=${KUBE_NAMESPACE} \
    ${HELMCHARTNAME} helm/charts/${HELMCHARTNAME} \
    -f helm/override-env-values/${ENV_NAME}/${HELMCHARTNAME}-values.yaml

  # above override values file help to override the configuration values per environment

    #rollback in case of failure
    if [ $? -ne 0 ]; then echo "Command failure! Unable to deploy ${HELMCHARTNAME}" exit 1; fi;
}

deployApp() {
  HELMCHARTNAME=$1

  docker run -i --rm \
    -v ${WORKDIR}:${WORKDIR}:ro \
    -v ${KUBE_CFGDIR}:${KUBE_CFGDIR}:ro \
    -e KUBE_URL -e KUBE_NAMESPACE -e KUBECONFIG \ # set kubernetes configs
    --network=host \
    alpine/helm:${HELM_VERSION} \
    upgrade --install \
    --namespace=${KUBE_NAMESPACE} \
    --set image.tag=${IMAGE_NAME} \
    ${HELMCHARTNAME} helm/charts/${HELMCHARTNAME} \
    -f helm/override-env-values/${ENV_NAME}/${HELMCHARTNAME}-values.yaml

  # above override values file help to override the configuration values per environment

    #rollback in case of failure
    if [ $? -ne 0 ]; then echo "Command failure! Unable to deploy ${HELMCHARTNAME}" exit 1; fi;
}

# Deploy database befoe application if its set to deploy true
if [ ! -z "${DEPLOY_DB}" ]; then
    deployDB ${DB_NAME}
    deployApp ${APPNAME}
else
    deployApp ${APPNAME}
fi;
