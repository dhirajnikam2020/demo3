#! /bin/bash -x
# @brief script use for rollback. In case of failure, will rollback to previous deployment


WORKDIR=$(pwd)
KUBE_CFGDIR=$(dirname $KUBECONFIG)

if [ -z "${HELMCHARTNAME}" ]; then
    HELMCHARTNAME=${APPNAME}
fi;

#rollback to previous deployment
echo "Rolling back deployment to previous version!!"
docker run -i --rm \
  -v ${WORKDIR}:${WORKDIR}:ro -w ${WORKDIR} \
  -v ${KUBE_CFGDIR}:${KUBE_CFGDIR}:ro \
  -e KUBE_URL -e KUBE_NAMESPACE -e KUBECONFIG \
  --network=host \
  alpine/helm:${HELM_VERSION} rollback ${HELMCHARTNAME} 0

exit 1
